<?php

namespace src;

ini_set('precision', 301);

class Task5
{
    public function main(int $n, array $arr = [0, 1])
    {
        if ($n < 0) {
            throw new \InvalidArgumentException();
        }

        do {
            $arr[] = $arr[count($arr) - 1] + $arr[count($arr) - 2];
        } while (floor(log10($arr[count($arr) - 1]) + 1) <= $n);

        return $arr[count($arr) - 1];
    }
}
