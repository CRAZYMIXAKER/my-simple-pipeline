<?php

namespace src;

class Task8
{
    public function main(string $json): string
    {
        if (!$this->isJSON($json)) {
            return throw new \InvalidArgumentException();
        }

        $arr = json_decode($json, true);
        $str = '';

        foreach ($arr as $key => $item) {
            if (is_array($item)) {
                foreach ($item as $keyTwo => $value) {
                    $str .= "$keyTwo: $value";
                }
            } else {
                $str .= "$key: $item\r\n";
            }
        }

        return $str;
    }

    public function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }
}
