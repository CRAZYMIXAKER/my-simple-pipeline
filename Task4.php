<?php

namespace src;

class Task4
{
    public function main(string $input): string
    {
        $output = preg_split('/[_ -]/ ', $input);
        foreach ($output as $key => $value) {
            $output[$key] = 0 == $key ? $value : mb_strtoupper($value[0]).substr($value, 1);
        }

        return implode($output);
    }
}
