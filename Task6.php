<?php

namespace src;

use DateTime;

class Task6
{
    public function main(int $year, int $lastYear, int $month, int $lastMonth, string $day = 'Monday'): array|int|string
    {
        if (!checkdate($month, 1, $year) || !checkdate($lastMonth, 1, $lastYear)) {
            throw new \InvalidArgumentException();
        }

        $days = [];
        $count = 0;
        $dateString = 'first '.$day.' of '.$year.'-'.$month;
        $lastDateString = 'last '.$day.' of '.$lastYear.'-'.$lastMonth;
        $startDay = new DateTime("$dateString");
        $finishDay = new DateTime("$lastDateString");

        while ($startDay->format('Y-m') <= $finishDay->format('Y-m')) {
            if ('Mon' == $startDay->format('D')) {
                $days[] = clone $startDay;
                ++$count;
            }
            $startDay->modify('+ 1 month');
        }
        $days['count'] = $count;

        return $days['count'];
    }
}
echo (new Task6())->main(1900, 2000, 2,3);