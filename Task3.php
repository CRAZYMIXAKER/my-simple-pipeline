<?php

namespace src;

class Task3
{
    public function main(int $number): int|string
    {
        if ($number < 0) {
            throw new \InvalidArgumentException();
        }

        if (strlen((string) $number) < 2) {
            throw new \InvalidArgumentException();
        }

        do {
            $numberToString = (string) $number;
            $count = 0;

            for ($i = 0; $i < strlen($numberToString); ++$i) {
                $count += (int) $numberToString[$i];
            }

            $number = (string) $count;
        } while (strlen($number) > 1);

        return (int) $number;
    }
}
