<?php

namespace src;

class Task9
{
    public function main(array $arr, int $number): array|string
    {
        $error = '';
        if (sizeof($arr) < 3) {
            throw new \InvalidArgumentException();
        }
        if ($number < 0) {
            throw new \InvalidArgumentException();
        }
        if (0 === $number) {
            throw new \InvalidArgumentException();
        }
        foreach ($arr as $item) {
            if ($item < 0) {
                throw new \InvalidArgumentException();
            }
        }

        if (empty($error)) {
            $output = [];
            for ($i = 0; $i < count($arr) - 2; ++$i) {
                $firstNumber = $arr[$i];
                $secondNumber = $arr[$i + 1];
                $thirdNumber = $arr[$i + 2];
                $number == ($firstNumber + $secondNumber + $thirdNumber) ?
                    $output[] = [$firstNumber.' + '.$secondNumber.' + '.$thirdNumber.' = '.$number] : '';
            }

            return $output;
        }

        return $error;
    }
}
