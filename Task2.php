<?php

namespace src;

use DateTime;

class Task2
{
    public function main($date): int|string
    {
        $dateNow = new DateTime('00:00');
        $dateArr = explode('-', $date);
        $day = $dateArr[2];
        $mont = $dateArr[1];
        $year = $dateArr[0];

        if (checkdate((int) $mont, (int) $day, (int) $year)) {
            $response = date_diff($dateNow, new DateTime("$date"))->days;

            if ($dateNow->format('Y-m-d') > $date) {
                throw new \InvalidArgumentException();
            }
        } else {
            throw new \InvalidArgumentException();
        }

        return $response;
    }
}
