<?php

namespace src;

class Task10
{
    public function main(int $input): array|string
    {
        $error = '';

        if (0 === $input) {
            throw new \InvalidArgumentException();
        }
        if ($input < 0) {
            throw new \InvalidArgumentException();
        }
        if (empty($error)) {
            $arr = [$input];
            do {
                if (0 == $input % 2) {
                    $input /= 2;
                } elseif ($input % 2 > 0) {
                    $input = $input * 3 + 1;
                }
                $arr[] = $input;
            } while (1 != $input);

            return $arr;
        }

        return $error;
    }
}
