<?php

namespace src;

class Task7
{
    public function main(array $arr, int $position): array|string
    {
        if ($position < 0) {
            throw new \InvalidArgumentException();
        }
        if ($arr) {
            if (count($arr) - 1 < $position) {
                throw new \InvalidArgumentException();
            }
            unset($arr[$position]);

            return array_values($arr);
        }

        throw new \InvalidArgumentException();
    }
}
